'use strict';

/* Controllers */

angular.module('myApp.controllers', ['ngResource'])
  .controller('MyCtrl1', ['$scope', '$resource', '$http', function($scope, $resource, $http) {
        //$scope.templateId = 'f1bf34b0013211e485395df50cf4733e';
        $scope.SERVER_URL = "https://dataserver-avantsoft.rhcloud.com/cardapp";
        $scope.templateDetails = null;
        var cardTemplateResource = $resource($scope.SERVER_URL + '/cardtpl/:id',
            {id : '@id'}
        );
        
        $scope.fetchTemplateInfo = function(card) {
            $scope.templateDetails = cardTemplateResource.get({id:card.cardTemplateId}, function(cardDetails) {
                
            });
        };
        
        $scope.saveCard = function() {
            $scope.templateDetails.$save({}, 
                function(response) {
                    alert("saved");
                }
            );
            /*cardTemplate.save({id:card.cardTemplateId}, function(cardDetails) {
                $scope.templateDetails = cardDetails;
            });*/
        };
        $scope.deleteCardTemplate = function(cardTemplate) {
            if(confirm("Are you sure you want to delete?")) {
                cardTemplateResource.delete({}, {'id': cardTemplate.cardTemplateId}, 
                    function(responseJson) {
                        alert(responseJson.message);
                    },
                    function(responseJson){
                        alert(responseJson.data.message);
                    }
                );
            }
        };
        $scope.fetchAllCards = function() {
            
            $http({method: 'GET', url:'https://dataserver-avantsoft.rhcloud.com/cardapp/cardtpl/appKey/53b1e0a79aba2b00009840f3'}).
                success(function (data, status, headers, config) {
                    $scope.allCards = data.cardTemplates;
                }).
                error(function (data, status, headers, config){
                     
                });
        };
  }])
  .controller('MyCtrl2', ['$scope', function($scope) {

  }]);
